# s3-proxy, a way to get private s3 buckets in a path based routing setup
FROM openresty/openresty:centos
MAINTAINER Stein van Broekhoven <stein@aapjeisbaas.nl>

# Install jq for compatibility with ecs role assuming
RUN yum install -y epel-release && \
    yum install -y jq

# Copy nginx configuration files
COPY nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
COPY ecs        /usr/bin/ecs

# Default index filename
ENV INDEX_FILENAME=index.html

# open a port
EXPOSE 8080
CMD ["sh","-c","source /usr/bin/ecs"] 
