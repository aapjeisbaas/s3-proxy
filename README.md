We want to have all our content behind **1 external dns entry** so we don't have to mess with **CORS**.
This proxy is solving one of the last pieces in this puzzle.
Getting S3 content to users via an ALB target group and ListenerRule to map it in the existing path based routing setup.


This container uses **IAM** roles/credentials in conjunction with nginx and lua to deliver content as fast as possible.

### Env Vars
| Key           | Example Val.  | Function |
| ------------- |---------------| ---------|
| BUCKET        | bucketname    | Bucket you wat to point |
| AWS_ACCESS    | AKIAIKJH...   | AWS access key          |
| AWS_SECRET    | UC6VFAu7we... | AWS secret key          |
| AWS_REGION    | eu-west-1     | Bucket origin region    |
| PATH_EXT      | /static/      | Path to call from the outside  |
| PATH_INT      | /master/b7e.. | Path in bucket where you want the external path to route traffic to including trailing '/' |
| INDEX_FILENAME| index-tst.html| Index filename to use when a directory is queried. When empty, the default index.html is used |

### How to use this without PATH_EXT
To use a subdir in s3 mapped to the root of your site:
```
PATH_EXT="/"
PATH_INT="/master/git7676revision/"
```

### Run it in ECS
The container can leverage the ecs role for authenticating against the s3 api to use it you need the following start cmd:
##### You don't need the `AWS_ACCESS` and `AWS_SECRET` when you use the ecs task role!

```
web console task config: 
sh,-c,source /usr/bin/ecs

docker equivalent: 
["sh","-c","source /usr/bin/ecs"] 
```
### What does the http path look like in our ecs setup?
#### REQ >> CDN >> ALB >> TG >> Container >> S3
 

### where else can I run this
Well it's a docker based x64 linux image so prety much everywhere here are some examples:
 - office dev env
 - local machine
 - azure
 - google compute
 - kubernetes


### Run it localy:
```
docker run -e BUCKET='bucketname' \
           -e AWS_ACCESS='********' \
           -e AWS_SECRET='**************' \
           -e AWS_REGION='eu-west-1' \
           -e PATH_EXT='static' \
           -e PATH_INT='master/b7e4f4aa7d2763e89cc5bb6a43a357d8d167446c' \
           registry.gitlab.com/nsi-public/s3-proxy:latest
```
